function filterBy(arr, dataType) {
    const filtered = arr.filter(item => typeof item !== dataType)
    return filtered;
}


const array = ['hello', 'world', 23, '23']
filterBy(array, 'number')